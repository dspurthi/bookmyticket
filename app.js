const express = require('express');
const exhbs = require('express-handlebars');
const bodyParser=require('body-parser');
const path = require('path');
var cors = require('cors');

const db=require('./config/database');
// test db
db.authenticate()
    .then(()=>console.log('Database Connected...'))
    .catch(err=> console.log('Error:'+err))

const app= express();
app.use(cors())
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/',(req,res)=>res.send('hello'));

app.use('/',require('./routes/routes_data'));

const PORT=process.env.PORT || 5000;

app.listen(PORT,console.log(`server listining on port ${PORT}`));