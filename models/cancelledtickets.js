const Sequelize = require("sequelize");
const db = require("../config/database");

const Seats=require("./seats");

const CancelledTickets = db.define(
  "cancelledtickets",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    seat_num:{
        type:Sequelize.INTEGER,
        references:{
            model:Seats,
            key:"seat_num",
        }
    },
    booking_id: {
      type: Sequelize.UUID,
      references: {
        model: Seats,
        key: "booking_id",
      },
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "cancelledtickets",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = CancelledTickets;