const Sequelize = require("sequelize");

const db = require("../config/database");
const Airlines = require("./airlines");
const Routes=require("./plane_routes");

const Planes = db.define(
  "planes",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    airline_id: {
        type: Sequelize.UUID,
        references: {
            model:  Airlines,
            key: "id",
          },
    },
    name:{
        type:Sequelize.STRING,
    },
    available_seats:{
        type:Sequelize.INTEGER
    },
    total_seats:{
      type:Sequelize.INTEGER
    }
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "planes",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Planes;