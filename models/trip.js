const Sequelize = require("sequelize");

const db = require("../config/database");
const Airlines = require("./airlines");
const Planes = require("./planes");
const Routes=require("./plane_routes");

const Trips = db.define(
  "trips",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    route_id: {
      type: Sequelize.UUID,
      references: {
        model: Routes,
        key: "id",
      },
    },
    plane_id: {
        type: Sequelize.UUID,
        references: {
            model:  Planes,
            key: "id",
          },
    },
    trip_date:{
        type:Sequelize.RANGE(Sequelize.DATE),
    },
    fare:{
      type:Sequelize.INTEGER,
    }
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "trips",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Trips;