const Sequelize = require("sequelize");

const db = require("../config/database");
const Planes = require("./planes");
const Trips = require("./trip");
const Users = require("./users");

const Bookings = db.define(
  "bookings",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
    },
    trip_id: {
      type: Sequelize.UUID,
      references: {
        model: Trips,
        key: "id",
      },
    },
    user_id: {
      type: Sequelize.UUID,
      references: {
        model: Users,
        key: "id",
      },
    },
    no_of_seats: {
      type: Sequelize.INTEGER,
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "bookings",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Bookings;
