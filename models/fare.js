const Sequelize = require("sequelize");

const db = require("../config/database");
const Airlines = require("./airlines");
const Planes = require("./planes");
const Routes=require("./plane_routes");
const Trips = require("./trip");

const Fares = db.define(
  "fares",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    trip_id: {
      type: Sequelize.UUID,
      references: {
        model: Trips,
        key: "id",
      },
    },
    fare:{
        type:Sequelize.INTEGER,
    }
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "fares",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Fares;