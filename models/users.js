const Sequelize = require("sequelize");

const db = require("../config/database");

const Users = db.define(
  "users",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    first_name: {
      type: Sequelize.STRING,
    },
    last_name: {
      type: Sequelize.STRING,
    },
    e_mail: {
      type: Sequelize.STRING,
    },
    phone_number: {
        type: Sequelize.INTEGER,
    },
    address: {
        type: Sequelize.STRING,
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "users",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Users;
