const Sequelize = require("sequelize");

const db = require("../config/database");
const Bookings=require("./bookings");

const Payments = db.define(
  "payments",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    booking_id: {
      type: Sequelize.UUID,
      references: {
        model: Bookings,
        key: "id",
      },
    },
    total_price:{
        type:Sequelize.INTEGER,
    }
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "payments",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Payments;