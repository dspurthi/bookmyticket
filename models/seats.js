const Sequelize = require("sequelize");
const db = require("../config/database");

const Bookings=require("./bookings");

const Seats = db.define(
  "seats",
  {
    seat_num: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    booking_id: {
      type: Sequelize.UUID,
      references: {
        model: Bookings,
        key: "id",
      },
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "seats",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Seats;