const Sequelize = require("sequelize");

const db = require("../config/database");

const Airlines = db.define(
  "airlines",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
    },
    location: {
        type: Sequelize.STRING,
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "airlines",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Airlines;