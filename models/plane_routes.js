const Sequelize = require("sequelize");

const db = require("../config/database");
const Airports = require("./airports");

const Routes = db.define(
  "routes",
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    source_airport_id: {
      type: Sequelize.STRING,
      references: {
        model: Airports,
        key: "code",
      },
    },
    destination_airport_id: {
        type: Sequelize.STRING,
        references: {
            model:  Airports,
            key: "code",
          },
    },
  },
  {
    schema: "ticketbooking",
    freezeTableName: true,

    // define the table's name
    tableName: "routes",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = Routes;