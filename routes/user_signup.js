const Users = require("../models/users");
const { v4: uuidv4 } = require('uuid');


const user=function(req, res) {
    Users.create({
      id: uuidv4(),
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      e_mail: req.body.e_mail,
      phone_number:req.body.phone_number,
      address:req.body.address
    })
      .then((users) => {
        res.json(users);
      })
      .catch((err) => console.log(err));
  };
  module.exports = user;