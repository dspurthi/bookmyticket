const Routes = require("../models/plane_routes");
const Airports=require("../models/airports")
const { v4: uuidv4 } = require('uuid');


const plane_routes=function(req, res) {
    Routes.create({
      id: uuidv4(),
      source_airport_id: req.body.source_airport_id,
      destination_airport_id: req.body.destination_airport_id
    })
      .then((plane_route) => {
        res.json(plane_route);
      })
      .catch((err) => console.log(err));
  };
  module.exports = plane_routes;