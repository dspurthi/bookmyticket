const Airlines = require("../models/airlines");
const { v4: uuidv4 } = require('uuid');


const airlines=function(req, res) {
    Airlines.create({
      id: uuidv4(),
      name: req.body.name,
      location:req.body.location
    })
      .then((airline) => {
        res.json(airline);
      })
      .catch((err) => console.log(err));
  };
  module.exports = airlines;