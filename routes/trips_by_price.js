const Trips = require("../models/trip");
const Planes = require("../models/planes");
const Routes = require("../models/plane_routes");
const { Op } = require("sequelize");
const Airports = require("../models/airports");

const trips_available_by_price = function (req, res) {
  const trip_promise = Trips.findAll({
    where: {
      fare: {
        [Op.lte]: req.body.price,
      },
    },
  });
  trip_promise.then((trips) => {
    // console.log(trips);
    const all_trips = [];
    trips.forEach((trip) => {
      const route_promise = Routes.findOne({
        where: {
          id: trip.route_id,
        },
      });
      all_trips.push(route_promise);
    }),
      Promise.all(all_trips).then((routes) => {
        // console.log(routes);
        const all_routes = [];
        routes.forEach((route) => {
          const airport_promise = Airports.findAll({
            where: {
              id: {
                [Op.in]: [
                  route.source_airport_id,
                  route.destination_airport_id,
                ],
              },
            },
          });
          all_routes.push(airport_promise);
        });
        Promise.all(all_routes).then((airports_couple) => {
          //   console.log(airports);
        //   const all_trips=[];
        //   airports.forEach((airport) => {
        //     const source = airport[0].location;
        //     const destination = airport[1].location;
        //     const result = {
        //       Source: source,
        //       Destination: destination,
        //     };
        //     all_trips.push(result)
        //   });
        const airports = airports_couple.flat();
        const response = getResponse(trips, routes, airports);
        res.json(response);
        });
      });
  });
};


const getResponse = function (trips, routes, airports) {
  const response = [];

  trips.forEach((trip_model) => {
    const trip = {};

    // First property
    trip.date = trip_model.trip_date;

    // Second and Third
    const route = routes.find((r) => r.id === trip_model.route_id);
    // console.log(route)
    // console.log(all_trips)

    const source_airport = airports.find(
      (ap) => ap.id === route.source_airport_id
    );
    const destination_airport = airports.find(
      (ap) => ap.id === route.destination_airport_id
    );

    // Add props
    // console.log(source_airport)
    trip.source = source_airport.location;
    trip.destination = destination_airport.location;

    // Push
    console.log(trip);
    response.push(trip);
  });

  return response;
};
module.exports = trips_available_by_price;
