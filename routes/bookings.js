const Bookings = require("../models/bookings");
const { Op } = require("sequelize");

const bookings = function (req, res) {
  Bookings.findAll({
    where: {
        user_id: req.params.user
        },
    })
    .then((booking) => {
        res.json(booking);
    })
    .catch((err) => console.log(err))

};

module.exports = bookings;
