const Users = require("../models/users");
const Trips = require("../models/trip");
const Routes = require("../models/plane_routes");
const Planes = require("../models/planes");
const Airlines = require("../models/airlines");
const Bookings = require("../models/bookings");
const { v4: uuidv4 } = require("uuid");
const { Op, where } = require("sequelize");
const trip = require("./trips");
const Payments = require("../models/payments");
const Airports = require("../models/airports");
const airports = require("./airports");

const finding_trips = function (req, res) {
  // Result structure
  const result = [];

  // Get trips
  const trips_promise = Trips.findAll({
    where: {
      plane_id: req.body.plane_id,
    },
  });

  trips_promise.then((trips) => {
    // Get Route Information
    routes_query = [];
    // Get the route information i.e source/destination airport ids
    trips.forEach((trip) => {
      const route_promise = Routes.findOne({
        where: {
          id: trip.route_id,
        },
      });

      // Push each route query to promise array
      routes_query.push(route_promise);
    });

    // On route information retrieval
    Promise.all(routes_query).then((routes) => {
      airports_couples_promises = [];

      // For each route get the source and destintion airport details
      routes.forEach((route) => {
        const airport_couple = Airports.findAll({
          where: {
            id: {
              [Op.in]: [route.source_airport_id, route.destination_airport_id],
            },
          },
        });

        // Push to promise array
        airports_couples_promises.push(airport_couple);
      });

      // On airport data retrieval
      Promise.all(airports_couples_promises).then((airport_couples) => {
        const airports = airport_couples.flat();
        const response = getResponse(trips, routes, airports);
        res.json(response);
      });
    });
  });
};

const getResponse = function (trips, routes, airports) {
  const response = [];

  trips.forEach((trip_model) => {
    const trip = {};

    // First property
    trip.date = trip_model.trip_date;

    // Second and Third
    const route = routes.find((r) => r.id === trip_model.route_id);

    const source_airport = airports.find(
      (ap) => ap.id === route.source_airport_id
    );
    const destination_airport = airports.find(
      (ap) => ap.id === route.destination_airport_id
    );

    // Add props
    trip.source = source_airport.location;
    trip.destination = destination_airport.location;

    // Push
    console.log(trip);
    response.push(trip);
  });

  return response;
};

module.exports = finding_trips;
