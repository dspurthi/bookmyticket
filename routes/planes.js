const Routes = require("../models/plane_routes");
const Airlines=require("../models/airlines")
const { v4: uuidv4 } = require('uuid');
const Planes = require("../models/PLANES.JS");


const planes=function(req, res) {
    Planes.create({
      id: uuidv4(),
      airline_id: req.body.airline_id,
      name:req.body.name,
      available_seats:req.body.available_seats
    })
      .then((plane) => {
        res.json(plane);
      })
      .catch((err) => console.log(err));
  };
  module.exports = planes;