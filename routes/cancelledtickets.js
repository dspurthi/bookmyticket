const { v4: uuidv4 } = require("uuid");
const Seats = require("../models/seats");
const CancelledTickets = require("../models/cancelledtickets");
const Bookings = require("../models/bookings");
const Trips = require("../models/trip");
const Planes = require("../models/planes");

const cancel_ticket = function (req, res) {
  const booking_promise = Bookings.findOne({
    where: {
      id: req.body.booking_id,
    },
  });
  booking_promise.then((booking) => {
    const no_of_seats = booking.no_of_seats;
    const trip_promise = Trips.findOne({
      where: {
        id: booking.trip_id,
      },
    });
    trip_promise.then((trip) => {
      const plane_promise = Planes.findOne({
        where: {
          id: trip.plane_id,
        },
      });
      plane_promise.then((plane) => {
        const available_seats = parseInt(plane.available_seats);
        // console.log(typeof plane.available_seats);
        // console.log(typeof req.body.no_of_seats);
        // res.json({});
        const update_plane = Planes.update(
          {
            available_seats: available_seats + req.body.no_of_seats,
          },

          {
            where: {
              id: plane.id,
            },
          }
        );
        update_plane.then((ticket) => {
          console.log(ticket);
          const update_booking = Bookings.update(
            {
              no_of_seats: no_of_seats - req.body.no_of_seats,
            },
            {
              where: {
                id: req.body.booking_id,
              },
            }
          );
          update_booking.then((cancel_ticket) => {
            Seats.destroy({
              where: {
                seat_num: req.body.seat_num,
              },
            })
              .then((cancelledseat) => {
                res.json(cancelledseat);
              })
              .catch((err) => console.log(err));
          });
        });
      });
    });
  });
};
module.exports = cancel_ticket;
