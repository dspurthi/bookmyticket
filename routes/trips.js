const Routes = require("../models/plane_routes");
const Airlines = require("../models/airlines");
const Trips = require("../models/trip");
const { v4: uuidv4 } = require("uuid");

// Timeline.create({
//   range: [new Date(Date.UTC(2021, 6, 1)), new Date(Date.UTC(2021, 7, 1))],
// });

// var Timeline = sequelize.define("Timeline", {
//   range: Sequelize.RANGE(Sequelize.DATE),
//   // [... some other columns here]
// });
const trip = function (req, res) {
  Trips.create({
    id: uuidv4(),
    route_id: req.body.route_id,
    plane_id: req.body.plane_id,
    trip_date: randomDate(new Date("06-1-2021"), new Date("07-01-2021")),
    fare: req.body.fare,
  })
    .then((trip) => {
      res.json(trip);
    })
    .catch((err) => console.log(err));
};

const randomDate = function (start, end) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
};

module.exports = trip;
