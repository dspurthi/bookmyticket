const Routes = require("../models/plane_routes");
const Trips=require("../models/trip");
const Fares=require("../models/fare");
const { v4: uuidv4 } = require('uuid');


const fare=function(req, res) {
    Fares.create({
      id: uuidv4(),
      trip_id: req.body.trip_id,
      fare:req.body.price
    })
      .then((fare) => {
        res.json(fare);
      })
      .catch((err) => console.log(err));
  };
  module.exports = fare;