const Users = require("../models/users");
const Trips = require("../models/trip");
const Routes = require("../models/plane_routes");
const Planes = require("../models/planes");
const Airlines = require("../models/airlines");
const Bookings = require("../models/bookings");
const { v4: uuidv4 } = require("uuid");
const { Op } = require("sequelize");
const trip = require("./trips");
const Payments = require("../models/payments");
const Airports = require("../models/airports");
const Seats=require("../models/seats")

// finding booking data from id

const booking_details = function (req, res) {
  const booking_promise = Bookings.findOne({
    where: {
      id: req.body.id,
    },
  });

  // finding user

  booking_promise.then((booking_item) => {
    const trip_id = booking_item.trip_id;
    const user_promise = Users.findOne({
      where: {
        id: booking_item.user_id,
      },
    });

    // finding trip using trip_id in bookings

    user_promise.then((user) => {
      const trip_promise = Trips.findOne({
        where: {
          id: trip_id,
        },
      });

      // finding plane from trip

      trip_promise.then((trip) => {
        const route_id = trip.route_id;
        const plane_promise = Planes.findOne({
          where: {
            id: trip.plane_id,
          },
        });
        // finding route

        plane_promise.then((plane) => {
          const route_promise = Routes.findOne({
            where: {
              id: route_id,
            },
          });
          // displaying booking Details

          route_promise.then((route) => {
            const airport_promise = Airports.findAll({
              where: {
                id: {
                  [Op.in]: [route.source_airport_id,route.destination_airport_id
                  ],
                },
              },
            });
            airport_promise.then((airports) => {
              const source=airports[0].code;
              const destination=airports[1].code;
              const payments_promise = Payments.findOne({
                where: {
                  booking_id: req.body.id,
                },
              });
              payments_promise.then((sale) => {
                const seat_promise=Seats.findAll({
                  where:{
                    booking_id:req.body.id
                  }
                });
                seat_promise.then((seats)=>{
                  const seat_nums = seats.map((i) => i.seat_num);
                  console.log(seats)
                  const result = {
                    User: user.first_name,
                    Source: source,
                    Destination:destination,
                    Date_of_Journey:trip.trip_date,
                    Plane: plane.name,
                    Amount:sale.total_price,
                    seat_nums:seat_nums
                  };
                  res.json(result);
                })
             
              });
            });
          });
        });
      });
    });
  });
};
module.exports = booking_details;
