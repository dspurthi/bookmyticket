const Airports = require("../models/airports");
const { v4: uuidv4 } = require('uuid');


const airports=function(req, res) {
    Airports.create({
      id: uuidv4(),
      name: req.body.name,
      location:req.body.location,
      code: req.body.code
    })
      .then((airport) => {
        res.json(airport);
      })
      .catch((err) => console.log(err));
  };
  module.exports = airports;