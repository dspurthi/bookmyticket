const Users = require("../models/users");
const Trips = require("../models/trip");
const Routes = require("../models/plane_routes");
const Planes = require("../models/planes");
const Airlines = require("../models/airlines");
const Bookings = require("../models/bookings");
const { v4: uuidv4 } = require("uuid");
const { Op, where } = require("sequelize");
const trip = require("./trips");
const Payments = require("../models/payments");
const Airports = require("../models/airports");
const airports = require("./airports");

const planes_of_route = function (req, res) {
  // Result structure
  const result = [];

  // Get trips
  const trips_promise = Trips.findAll({
    where: {
      route_id: req.body.route_id,
    },
  });

  trips_promise.then((trips) => {
    // console.log(trips)
    // Get Route Information
    planes_query = [];
    // Get the route information i.e source/destination airport ids
    trips.forEach((plane) => {
      const plane_promise = Planes.findOne({
        where: {
          id: plane.plane_id,
        },
      });

      // Push each route query to promise array
      planes_query.push(plane_promise);
    });

    // On route information retrieval
    Promise.all(planes_query).then((planes) => {
      // console.log(planes);
      route_promise = Routes.findOne({
        where: {
          id: req.body.route_id,
        },
      });
      route_promise.then((route) => {
        // console.log(route.source_airport_id)
        // console.log(route.destination_airport_id)
        const airport_couple = Airports.findAll({
          where: {
            id: {
              [Op.in]: [route.source_airport_id, route.destination_airport_id],
            },
          },
        });
        airport_couple.then((route_data) => {
          // const From=route_data[0].name
          // const To=route_data[1].name
          const response = getResponse(trips, planes, route_data);
          res.json(response);
        });
      });
    });
  });
};

const getResponse = function (trips, planes, route_data) {
  const response = [];

  trips.forEach((trip_model) => {
    const trip = {};

    // First property
    trip.date = trip_model.trip_date;

    // Second and Third
   const plane= planes.find((p) => p.id === trip_model.plane_id);
  //  console.log(plane)

    const source_airport = route_data[0].location
    const destination_airport = route_data[1].location

    // Add props
    trip.source = source_airport;
    trip.destination = destination_airport;
    trip.plane=plane.name;

    // Push
    console.log(trip);
    response.push(trip);
  });

  return response;
};

module.exports = planes_of_route;
