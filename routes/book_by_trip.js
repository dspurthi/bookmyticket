const Users = require("../models/users");
const Trips = require("../models/trip");
const Routes = require("../models/plane_routes");
const Planes = require("../models/planes");
const Airlines = require("../models/airlines");
const Bookings = require("../models/bookings");
const { v4: uuidv4 } = require("uuid");
const Payments = require("../models/payments");
const Seats = require("../models/seats");

const book_by_trip = function (req, res) {
  Trips.findOne({
    where: {
      id: req.body.trip_id,
    },
  }).then((trip) => {
    const plane_id = trip.plane_id;
    const price = trip.fare;
    console.log(price);
    Planes.findOne({
      where: {
        id: plane_id,
      },
    }).then((plane) => {
      console.log(plane)
      const post_available_seats = plane.available_seats - req.body.seats;
      const starting_seat_num = post_available_seats+1;
      if (post_available_seats > 0) {
        Bookings.create({
          id: uuidv4(),
          trip_id: req.body.trip_id,
          user_id: req.body.user,
          no_of_seats: req.body.seats,
        }).then((ticket) => {
          // console.log(ticket);
          Planes.update(
            {
              available_seats: post_available_seats,
            },
            {
              where: {
                id: plane_id,
              },
            }
          ).then((updatedplanes) => {
            // console.log(updatedplanes)
            const requests = [];
            
            console.log(starting_seat_num);
            for (let index = 0; index < req.body.seats; index++) {
              const seats_promise = Seats.create({
                seat_num: starting_seat_num + index,
                booking_id: ticket.id,
              });
              requests.push(seats_promise);
            }

            Promise.all(requests).then((seats) => {
              Payments.create({
                id: uuidv4(),
                booking_id: ticket.id,
                total_price: req.body.seats * price,
              }).then((receipt) => {
                res.json(receipt);
              });
            });
          });
        });
      } else {
        res.json("Seats not Available!!!!!!!");
      }
    });
  });
};
module.exports = book_by_trip;
